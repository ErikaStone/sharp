﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopItems
{
    interface IShop
    {
        public void AddProduct(Item product);
        public void ShowEatable();
        public void ShowUneatable();
        public void ShowDearestItem();
        public void ShowCheapest();
    }
}
