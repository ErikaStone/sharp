﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopItems
{
    public class Pencil : Uneatable
    {
        public Pencil(string name, float cost)
        {
            this.Name = name;
            this.Cost = cost;
        }
        public void Drow()
        {
            Console.WriteLine("Творит своими чернилами чертёж)");
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}");
        }
    }
}
