﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopItems
{
    public class Meet : Eatable
    {
        public Meet(string name, float cost)
        {
            this.Name = name;
            this.Cost = cost;
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}");
        }
    }
}
